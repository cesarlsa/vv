using System.ComponentModel.DataAnnotations;

namespace VV.Models
{
    public class Usuario
    {
        [Key]
        public int ID { get; set; }
        
        [Required]
        [StringLength(256, ErrorMessage = "O Nome deve ter no máximo 256 caracteres.")]
        public string Nome { get; set; }
        
        [Required]
        [StringLength(255, ErrorMessage = "O E-mail deve ter no máximo 255 caracteres.")]
        [RegularExpression(@"b[A-Z0-9._%-]+@[A-Z0-9.-]+.[A-Z]{2,4}b", ErrorMessage = "E-mail em formato inválido.")]
        public string Email { get; set; }
        
        [Required]
        [StringLength(42)]
        public string SenhaHash { get; set; }
    }
}