using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VV.Models;
using System.Text;

namespace VV.Controllers
{
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly UsuarioContext _context;
        private readonly string salt = "CESAR"; 

        public UsuarioController(UsuarioContext context) {
            _context = context;
        }

        // GET api/usuario
        [HttpGet]
        [Route("api/[controller]")]
        public async Task<IActionResult> Get()
        {
            try
            {
                var usuarios = from u in _context.Usuario select u;
                await usuarios.ForEachAsync(u => u.SenhaHash = string.Empty);
                return Ok(await usuarios.ToListAsync<Usuario>());
            }
            catch (Exception ex)
            {
                //LogException(ex);
                return StatusCode(500);
            }
        }

        // POST api/login
        [HttpPost]
        [Route("api/login")]
        public async Task<IActionResult> Post(string email, string senha)
        {
            try
            {
                string senhaHash = HashSenha(senha);

                var usuarios = from u in _context.Usuario select u;
                var usuario = await usuarios.FirstOrDefaultAsync(u => u.Email.Equals(email) && u.SenhaHash.Equals(senhaHash));

                if (usuario == null)
                    return NotFound();
                else
                    return Ok(usuario);
            }
            catch (Exception ex)
            {
                //LogException(ex);
                return StatusCode(500);
            }
        }

        // PUT api/usuario/5
        [HttpPut()]
        [Route("api/[controller]")]
        public async Task<IActionResult> Put(string nome, string email, string senha)
        {
            try
            {
                var usuarios = from u in _context.Usuario select u;
                var usuario = await usuarios.FirstOrDefaultAsync(u => u.Email.Equals(email));

                if (usuario == null)
                {
                    Usuario newUsuario = new Usuario { Nome = nome, Email = email, SenhaHash = HashSenha(senha)};
                    _context.Add(newUsuario);
                    await _context.SaveChangesAsync();
                    return Ok(newUsuario);
                }
                else
                {
                    return StatusCode(409);
                }
            }
            catch (Exception ex)
            {
                //LogException(ex);
                return StatusCode(500);
            }
        }

        // PATCH api/usuario/5
        [HttpPatch()]
        [Route("api/[controller]/{id}")]
        public async Task<IActionResult> Patch(int id, string nome, string email, string senha)
        {
            try
            {
                var usuario = await GetUsuario(id);
                if (usuario == null)
                {
                    return NotFound();
                }
                else
                {
                    usuario.Nome = nome;
                    usuario.Email = email;
                    usuario.SenhaHash = HashSenha(senha);
                    _context.Update(usuario);
                    await _context.SaveChangesAsync();
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                //LogException(ex);
                return StatusCode(500);
            }
        }

        // DELETE api/usuario/5
        [HttpDelete()]
        [Route("api/[controller]/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var usuario = await GetUsuario(id);
                if (usuario == null)
                {
                    return NotFound();
                }
                else
                {
                    _context.Remove(usuario);
                    await _context.SaveChangesAsync();
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                //LogException(ex);
                return StatusCode(500);
            }
        }

        private string HashSenha(string senha)
        {
            var sha1 = SHA1.Create();
            var buffer = sha1.ComputeHash(Encoding.ASCII.GetBytes(senha + salt));
            return Encoding.UTF8.GetString(buffer, 0, buffer.Length);
        }

        private async Task<Usuario> GetUsuario(int id)
        {
            var usuarios = from u in _context.Usuario select u;
            return await usuarios.FirstAsync(u => u.ID == id);
        }
    }
}