﻿$(document).ready(function() {
    $(function() {
        $(".btn-entrar").click(function() {
            const email = $("#email").val();
            const senha = $("#password").val();
            const url = "/api/login?email=" + email + "&senha=" + senha;
            $.ajax({
                type: "post",
                url: url,
                success: function(responseData, textStatus, jqXHR) {
                    const usuario = responseData;
                    $.ajax({
                        type: "get",
                        url: "/atualizar.html",
                        success: function(responseData, textStatus, jqXHR) {
                            $("#formData").html(responseData);
                            $("#id").val(usuario.id);
                            $("#nome").val(usuario.nome);
                            $("#email").val(usuario.email);
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log(errorThrown);
                        }
                    })
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                    $("#divLoginInvalido").css("visibility", "visible");
                    setTimeout(function(){$("#divLoginInvalido").css("visibility", "hidden");}, 5000);
                }
            })
        })
        $(".btn-atualizar").click(function() {
            const id = $("#id").val();
            const nome = $("#nome").val();
            const email = $("#email").val();
            const senha = $("#password").val();
            const url = "/api/usuario/" + id + "?nome=" + nome + "&email=" + email + "&senha=" + senha;
            $.ajax({
                type: "patch",
                url: url,
                success: function(responseData, textStatus, jqXHR) {
                    const usuario = responseData;
                    $.ajax({
                        type: "get",
                        url: "/atualizar.html",
                        success: function(responseData, textStatus, jqXHR) {
                            $("#formData").html(responseData);
                            $("#id").val(usuario.id);
                            $("#nome").val(usuario.nome.toString().trim());
                            $("#email").val(usuario.email.toString().trim());
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log(errorThrown);
                            $("#divLoginInvalido").css("visibility", "visible");
                        }
                    })
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            })
        })
        $(".btn-excluir").click(function() {
            $("#divAtualizar").css("visibility", "visible");
            $("#divAtualizar").html('<h4>Teste</h4>');
            // $("#divAtualizar").html('<div class=row">Confirma Exclusão</div><div class=row><button type="button" class="btn btn-primary btn-confirma">Sim</button><button type="button" class="btn btn-primary btn-naoconfirma">Não</button></div>");
        })
        $(".btn-naoconfirma").click(function() {
            $("#divAtualizar").css("visibility", "hidden");
        })
        $(".btn-confirma").click(function() {
            const id = $("#id").val();
            const url = "/api/usuario/" + id;
            $.ajax({
                type: "delete",
                url: url,
                success: function(responseData, textStatus, jqXHR) {
                    $("#divAtualizar").html("<h4>Excluido com sucesso</h4>");
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            })
        })
        // $(".link-cadastrar").click(function() {
        // })
    })
});

function Incluir() {
    $.ajax({
        type: "get",
        url: "/inclusao.html",
        success: function(responseData, textStatus, jqXHR) {
            $("#formData").html(responseData);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
            $("#divLoginInvalido").css("visibility", "visible");
        }
    })
}