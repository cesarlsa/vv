$(document).ready(function() {
    $(function() {
        $(".btn-inserir").click(function() {
            const nome = $("#nome").val();
            const email = $("#email").val();
            const senha = $("#password").val();
            const url = "/api/usuario/?nome=" + nome + "&email=" + email + "&senha=" + senha;
            $.ajax({
                type: "put",
                url: url,
                success: function(responseData, textStatus, jqXHR) {
                    $(".btn-inserir").attr("disabled", true); 
                    $("#divMensagem").css("visibility", "visible");
                    $("#divMensagem").html("<h4>Cadastro com sucesso!</h4>");
                    setTimeout(function() { location.reload(); }, 5000);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                    $("#divMensagem").css("visibility", "visible");
                    if (errorThrown=='Conflict')
                        $("#divMensagem").html("<h4>E-mail já cadastrado!</h4>");
                    else
                    $("#divMensagem").html("<h4>Erro no cadastro!</h4>");
                    setTimeout(function(){$("#divMensagem").css("visibility", "hidden");}, 5000);
                }
            })
        })
    })
});