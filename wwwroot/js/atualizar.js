$(document).ready(function() {
    $(function() {
        $(".btn-atualizar").click(function() {
            const id = $("#id").val();
            const nome = $("#nome").val();
            const email = $("#email").val();
            const senha = $("#password").val();
            const url = "/api/usuario/" + id + "?nome=" + nome + "&email=" + email + "&senha=" + senha;
            $.ajax({
                type: "patch",
                url: url,
                success: function(responseData, textStatus, jqXHR) {
                    $("#divMensagem").css("visibility", "visible");
                    $("#divMensagem").html("<h4>Alterado com sucesso!</h4>");
                    setTimeout(function() { location.reload(); }, 5000);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                    $("#divMensagem").css("visibility", "visible");
                    $("#divMensagem").html("<h4>Erro na alteração!</h4>");
                    setTimeout(function(){$("#divMensagem").css("visibility", "hidden");}, 5000);
                }
            })
        })
        $(".btn-excluir").click(function() {
            $(".btn-excluir").attr("disabled", true); 
            $(".btn-atualizar").attr("disabled", true); 
            $("#divAtualizar").css("visibility", "visible");
        })
        $(".btn-naoconfirma").click(function() {
            $(".btn-excluir").attr("disabled", false); 
            $(".btn-atualizar").attr("disabled", false); 
            $("#divAtualizar").css("visibility", "hidden");
        })
        $(".btn-confirma").click(function() {
            const id = $("#id").val();
            const url = "/api/usuario/" + id;
            $.ajax({
                type: "delete",
                url: url,
                success: function(responseData, textStatus, jqXHR) {
                    $(".btn-excluir").attr("disabled", true); 
                    $(".btn-atualizar").attr("disabled", true); 
                    $("#divAtualizar").css("visibility", "hidden");
                    $("#divMensagem").css("visibility", "visible");
                    $("#divMensagem").html("<h4>Excluido com sucesso</h4>");
                    setTimeout(function() { location.reload(); }, 5000);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                    $("#divMensagem").css("visibility", "visible");
                    $("#divMensagem").html("<h4>Erro na exclusão!</h4>");
                    setTimeout(function(){$("#divMensagem").css("visibility", "hidden");}, 5000);
                }
            })
        })
    })
});